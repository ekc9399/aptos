# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=lemmy-ui
pkgver=0.18.3
_translations_commit="$( . $startdir/../../*/lemmy/APKBUILD; echo $_translations_commit )"
pkgrel=0
pkgdesc="Link aggregator and forum for the Fediverse - Official webapp"
url="https://join-lemmy.org/"
# ppc64le: vips not available
# riscv64: RuntimeError: memory access out of bounds
# x86: Zone Allocation failed - process out of memory
# armhf, armv7, s390x: follow lemmy aport
arch="aarch64 x86_64"
license="AGPL-3.0-only"
depends="nodejs"
makedepends="npm vips-dev yarn"
checkdepends="cmd:start-stop-daemon curl"
install="$pkgname.pre-install"
pkgusers="lemmy-ui"
pkggroups="lemmy-ui"
subpackages="$pkgname-openrc"
source="https://github.com/LemmyNet/lemmy-ui/archive/$pkgver/lemmy-ui-$pkgver.tar.gz
	https://github.com/LemmyNet/lemmy-translations/archive/$_translations_commit/lemmy-translations-$pkgver.tar.gz
	lemmy-ui.confd
	lemmy-ui.initd
	"

# Workaround for bad gyp usage causing 'stat64' errors
export CFLAGS="$CFLAGS -D_LARGEFILE64_SOURCE"
export CPPFLAGS="$CPPFLAGS -D_LARGEFILE64_SOURCE"
export CXXFLAGS="$CXXFLAGS -std=c++17 -D_LARGEFILE64_SOURCE"

prepare() {
	default_prepare

	rmdir -v lemmy-translations
	ln -sv "$srcdir/lemmy-translations-$_translations_commit" \
		lemmy-translations

	sed -i "s/unknown version/$pkgver/" src/shared/version.ts
	yarn install --frozen-lockfile --production
}

build() {
	export GIT_DIR="$builddir"
	NODE_ENV=production yarn build:prod
	unset GIT_DIR
}

check() {
	start-stop-daemon -Sbmp "$startdir"/test.pid -w 5000 \
		-e 'NODE_ENV=production' -x node dist/js/server.js
	curl -o "$startdir"/test.out http://127.0.0.1:1234 || true
	start-stop-daemon -Kp "$startdir"/test.pid
	grep -q "UI: $pkgver" "$startdir"/test.out
}

package() {
	mkdir -p "$pkgdir"/usr/share/webapps/$pkgname
	cp -r dist node_modules "$pkgdir"/usr/share/webapps/$pkgname/

	# cleanup unused files
	find "$pkgdir" -type f -a \( \
		-name "*.ts" \
		-o -name "webpack*" \
		-o -name "tsconfig*" \
		-o -name "babel.config*" \
		-o -name "README*" \
		-o -name "readme*" \
		-o -name "CHANGELOG*" \
		-o -name "*.map" \
		-o -name "LICENSE*" \
		-o -name "License" \
		-o -name "license" \
		-o -name "*.md" \
		\) \
		-delete
	find "$pkgdir" -type d -a \( \
		-name "example" \
		-o -name "examples" \
		-o -name "test" \
		-o -name "tests" \
		-o -name ".github" \
		\) \
		-exec rm -rf '{}' \+

	install -Dm644 "$srcdir"/lemmy-ui.confd "$pkgdir"/etc/conf.d/lemmy-ui
	install -Dm755 "$srcdir"/lemmy-ui.initd "$pkgdir"/etc/init.d/lemmy-ui
}

sha512sums="
a5a2ad593b8f3fc2a07f848e35f5e81725f02df7aa5757b8a5c334a1a5cd99ed99ed5be4d91b70b1d2987a4b192c7234f6791c38dcbf52b035bd0adda28755b3  lemmy-ui-0.18.3.tar.gz
e733818a5f91eb0686a08f97dd012b52beb99371e51742ce8edf4d27e121ea6e244aa90f4198b7ac4b5ec4bad75f00340e1570525ce3d98caf13cb53f08cbc87  lemmy-translations-0.18.3.tar.gz
cac55baba8fa7cd49e41877e80296b1505aee35444d025c6c612a5c7091f36c92adeab17117d8f58d228f7bbe9f016a9e752a8b9c0f9a940bd8980132577e1a9  lemmy-ui.confd
59559ffab27cd9562b0f920a72df0a66676f96612960c573132ef1d8914d16ff36300e2514c0fd8e3d16ffd13af58fc129afc88b14f64a16468457e224b97fbd  lemmy-ui.initd
"
