# Contributor: Sam Nystrom <sam@samnystrom.dev>
# Maintainer: Sam Nystrom <sam@samnystrom.dev>
pkgname=lutgen
pkgver=0.8.1
pkgrel=0
pkgdesc="Blazingly fast interpolated LUT generator and applicator for arbitrary and popular color palettes"
url="https://github.com/ozwaldorf/lutgen-rs"
arch="all"
license="MIT"
makedepends="cargo cargo-auditable"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/ozwaldorf/lutgen-rs/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/lutgen-rs-$pkgver"
options="net" # cargo fetch

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
	mkdir -p completions
	./target/release/lutgen completions bash > completions/lutgen
	./target/release/lutgen completions zsh > completions/_lutgen
	./target/release/lutgen completions fish > completions/lutgen.fish
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/lutgen "$pkgdir"/usr/bin/lutgen
	install -Dm644 completions/lutgen "$pkgdir"/usr/share/bash-completion/completions/lutgen
	install -Dm644 completions/_lutgen "$pkgdir"/usr/share/zsh/site-functions/_lutgen
	install -Dm644 completions/lutgen.fish "$pkgdir"/usr/share/fish/vendor_completions.d/lutgen.fish
}

sha512sums="
2bc95aec6423c1c1cad235a1ad141198167f572eb254dee4a32b8e71c5d2af8b0b31a749e8eb5c003a8a90e5cb12db9c5f7539682553e4527c4fa0291ed0cc0e  lutgen-0.8.1.tar.gz
"
