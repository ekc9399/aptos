# Contributor: Fabio Ribeiro <fabiorphp@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php82-pecl-amqp
_extname=amqp
pkgver=2.0.0_alpha1
_pkgver=${pkgver/_/}
pkgrel=0
pkgdesc="PHP 8.2 extension to communicate with any AMQP spec 0-9-1 compatible server - PECL"
url="https://pecl.php.net/package/amqp"
arch="all"
license="PHP-3.01"
_phpv=82
_php=php$_phpv
depends="$_php-common"
makedepends="$_php-dev rabbitmq-c-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$_pkgver.tgz"
builddir="$srcdir/$_extname-$_pkgver"

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=php-config$_phpv
	make
}

check() {
	# Tests require running AMQP server, so basic check
	$_php -d extension="$builddir"/modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/$_php/conf.d
	mkdir -p $_confdir
	echo "extension=$_extname" > $_confdir/40_$_extname.ini
}

sha512sums="
057296c161bb676bf2a1b4dac962d6846792f769f794fa70261992299c9a37310da23368349191e104a9e0d9a4c6b916e362c763bf7918c17d8fce78a216ff72  php-pecl-amqp-2.0.0_alpha1.tgz
"
