# Maintainer:
pkgname=font-iosevka
pkgver=25.1.1
pkgrel=1
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
584179b70139c27f64f0bff4b426ad2db06878b8fe416e0e2501f49a4bb3ff76c94a2a2cc6c08d61f8e00aecca164e181e13040701071ed4352455a0d0eb4f27  super-ttc-iosevka-25.1.1.zip
38ea2957c6af5529c8c275b1d77a7d1ee41c00aa4e0bdffaea781ecac072e37e1bf8a9cf96b4ffe3f3165a75ad85979b132e06edfd1c3f38f75b3b72486cfa07  super-ttc-iosevka-aile-25.1.1.zip
85942cbd61186caac7321429f7936e8f273d0f05012bd3f2f65bf4f27492adc2de9ebea869cd524254bd1aaab2f6d9d1208ddec4594f14026a7ead82c8579596  super-ttc-iosevka-slab-25.1.1.zip
d78c8c647768c18241bd9f6255fac85af73c1937fe5017890063a186467216fae48f40f60057441dd059e81de712f1d247ea44959ba240f1a5a735734869366e  super-ttc-iosevka-curly-25.1.1.zip
044cf8f5e06f1667667e0ba0e1a2f4fc55cf86d0ff2a22736191e35f3e7032c36383d00af644ed237c5a6904d0031b9bda4ed266479c37c83495c9dfe960ebd8  super-ttc-iosevka-curly-slab-25.1.1.zip
"
