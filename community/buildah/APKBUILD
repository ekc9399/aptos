# Contributor: kohnish <kohnish@gmx.com>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=buildah
pkgver=1.31.1
pkgrel=0
pkgdesc="tool that facilitates building OCI container images"
url="https://github.com/containers/buildah"
license="Apache-2.0"
arch="all"
depends="oci-runtime shadow-subids slirp4netns containers-common"
makedepends="go go-md2man lvm2-dev gpgme-dev libseccomp-dev btrfs-progs-dev bash"
subpackages="$pkgname-doc"
options="!check" # tests require root privileges
source="https://github.com/containers/buildah/archive/v$pkgver/buildah-$pkgver.tar.gz"

# secfixes:
#   1.28.0-r0:
#     - CVE-2022-2990
#   1.21.3-r0:
#     - CVE-2021-3602
#   1.19.4-r0:
#     - CVE-2021-20206
#   1.14.4-r0:
#     - CVE-2020-10696

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	GIT_COMMIT="$pkgver" make
}

package() {
	GIT_COMMIT="$pkgver" make install PREFIX=/usr DESTDIR="$pkgdir"
}

sha512sums="
7375877d964197d0690542e1da0636b0a67cdf01f30ddbdd69cced5cfe4f8bb370b37ec58f2dffd2c2f048b897470d8fb06cd9f70c8e75df2aa6b19f86610f7b  buildah-1.31.1.tar.gz
"
