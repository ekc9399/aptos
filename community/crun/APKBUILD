# Contributor: kohnish <kohnish@gmx.com>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=crun
pkgver=1.8.6
pkgrel=0
pkgdesc="Fast and lightweight fully featured OCI runtime and C library for running containers"
url="https://github.com/containers/crun"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
arch="all"
makedepends="libcap-dev libseccomp-dev yajl-dev argp-standalone python3 go-md2man"
subpackages="$pkgname-doc $pkgname-static"
source="https://github.com/containers/crun/releases/download/$pkgver/crun-$pkgver.tar.xz"

provides="oci-runtime"
provider_priority=100 # highest, default provider

# secfixes:
#   1.4.4-r0:
#     - CVE-2022-27650

build() {
	./configure \
		--prefix=/usr \
		--disable-systemd
	make
}

check() {
	make tests/tests_libcrun_errors.log
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
d527b58ce4d5a7937260cc1336e10997595fed774e0faf4fed90e783de1ff4e3f036d800c7b92173f7fcad8bb1c0d6ee01989534d9b43eb0b937eef46a335f7d  crun-1.8.6.tar.xz
"
