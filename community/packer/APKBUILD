# Maintainer: Galen Abell <galen@galenabell.com>
# Contributor: Galen Abell <galen@galenabell.com>
pkgname=packer
pkgver=1.9.2
pkgrel=0
pkgdesc="tool for creating machine images for multiple platforms"
url="https://www.packer.io/"
license="MPL-2.0"
arch="all"
makedepends="go"
options="net chmod-clean"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/packer/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -trimpath"

build() {
	export GOLDFLAGS="-X github.com/hashicorp/packer/version.Version=$pkgver -X github.com/hashicorp/packer/version.VersionPrerelease="
	go build -v -o bin/$pkgname -ldflags="$GOLDFLAGS"
}

check() {
	go list . | xargs -t -n4 go test -timeout=2m -parallel=4
	bin/$pkgname -v
}

package() {
	install -Dm755 bin/"$pkgname" -t "$pkgdir"/usr/bin/
}

sha512sums="
e0d246571a5bac72a40eb1cea00572119bb0b3b5d8b3b40a74e0e47baca713b25fe3fd81a8e799ca5ec83be3d6d9d1be1b1e2309e3119f290c216a1a3c8db398  packer-1.9.2.tar.gz
"
